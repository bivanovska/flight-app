import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from 'angularfire2/auth';
import { FormControl, FormGroup, Validators} from '@angular/forms';
import { AuthService } from '../services/auth.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {

  constructor(private auth: AuthService) { }

    form = new FormGroup({
        email: new FormControl('', [ Validators.required, Validators.email]),
        password: new FormControl('', [ Validators.required, Validators.minLength(6)])
    });


  signIn() {
    this.auth.signIn(this.form.get('email').value, this.form.get('password').value);
  }

}
