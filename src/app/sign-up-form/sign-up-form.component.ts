import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import * as firebase from 'firebase';
import { AuthService } from '../services/auth.service';
@Component({
  selector: 'app-sign-up-form',
  templateUrl: './sign-up-form.component.html',
  styleUrls: ['./sign-up-form.component.css']
})
export class SignUpFormComponent {
  constructor(private auth: AuthService) {}

    form = new FormGroup({
    name: new FormControl('', Validators.required),
    surname: new FormControl('', Validators.required),
    email: new FormControl('', [ Validators.required, Validators.email]),
    username: new FormControl('', Validators.required),
    password: new FormControl('', [ Validators.required, Validators.minLength(6)])
  });

  get email() {
    return this.form.get('email');
  }

  get password() {
    return this.form.get('password');
  }

  get username() {
    return this.form.get('username');
  }

  signUp() {
    this.auth.signUp(this.email.value, this.password.value);
  }
}
