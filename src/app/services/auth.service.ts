import { Injectable, Input } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  currentUser$: Observable<firebase.User>;

  constructor(private afAuth: AngularFireAuth,
    private router: Router) {
      this.currentUser$ = afAuth.authState;
    }

  signIn(email, password) {
    this.afAuth.auth.signInWithEmailAndPassword(email, password).then(
      user => {
        this.router.navigateByUrl('/search-flights');
      }
    ).catch(function(error) {
      // Handle Errors here.
      const errorCode = error.code;
      const errorMessage = error.message;
      alert(errorCode + '---' + errorMessage);
    });
  }

  signUp(email, password) {
    this.afAuth.auth.createUserWithEmailAndPassword(email, password)
    .then(
      user => {
        this.router.navigate(['/search-flights']);
      }
    ).catch(function(error) {
      // Handle Errors here.
      const errorCode = error.code;
      const errorMessage = error.message;
      alert(errorCode + '---' + errorMessage);
    });
  }

  signOut() {
    this.afAuth.auth.signOut();
    this.router.navigate(['/']);
  }
}
