import { Injectable } from '@angular/core';
import { Http, RequestOptionsArgs } from '@angular/http';
import { HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FlightService {
  paramsListMap: Map<string, string> = new Map<string, string>();

   private baseUrl = 'http://developer.goibibo.com/api/search/?' +
   'app_id=619b028c&app_key=73f1f7c746cf42159859d1de3ebfb5ec&format=json';
   constructor(private http: Http) {
     this.paramsListMap.set('', '');
    }

   getFlights(form) {
     const params = this.buildParams(form);
     return this.http.get(this.baseUrl, {params});
   }

   private buildParams(form) {
    return new HttpParams()
    .set('source', form.get('sourceAirport').value).set('destination', form.get('destinationAirport').value)
    .set('dateofdeparture', form.get('departureDate').value).set('dateofarrival', form.get('arrivalDate').value)
    .set('adults', form.get('adults').value).set('children', form.get('children').value)
    .set('seatingclass', 'E').set('counter', '0')
    .toString();
   }
}
