import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {
 url = 'http://api.openweathermap.org/data/2.5/weather?appid=b1b15e88fa797225412429c1c50c122a1';
  constructor(private http: Http) { }

  getCurrentWeatherFor(city) {
    const param =  new HttpParams()
    .set('q', city).toString();
  return this.http.get(this.url, param);
  }
}
