import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AngularFireModule} from 'angularfire2';
import { AngularFireDatabaseModule} from 'angularfire2/database';
import { AngularFireAuthModule} from 'angularfire2/auth';
import { DataTableModule} from 'angular5-data-table';
import { AppComponent } from './app.component';
import { environment } from '../environments/environment';
import { SearchFormComponent } from './search-flights/search-flights.component';
import { SignUpFormComponent } from './sign-up-form/sign-up-form.component';
import { HttpModule} from '@angular/http';
import {RouterModule} from '@angular/router';
import { HomeComponent } from './home/home.component';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { ValuesPipe } from './search-flights/values.pipe';
import { AuthService } from './services/auth.service';
import { AuthGuard } from './services/auth-guard.service';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SearchFormComponent,
    SignUpFormComponent,
    NavBarComponent,
    ValuesPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    RouterModule.forRoot([
      {path: '', component: HomeComponent},
      {path: 'sign-up', component: SignUpFormComponent},
      {path: 'search-flights', component: SearchFormComponent, canActivate: [AuthGuard]}
    ]),
    DataTableModule
  ],
  providers: [
    AuthService,
    AuthGuard,
    ValuesPipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
