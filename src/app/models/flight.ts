export interface Flight {
  airline: string;
  depdate: string;
  arrtime: string;
  stops: string;
  operatingcarrier: string;
  flightno: string;

}
