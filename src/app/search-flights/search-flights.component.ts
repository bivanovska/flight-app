import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { Http } from '@angular/http';
import { HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FlightService } from '../services/flight.service';
import { DataTableResource } from 'angular5-data-table';
import { Flight } from '../models/flight';
import { WeatherService } from '../services/weather.service';


@Component({
  selector: 'app-search-flights',
  templateUrl: './search-flights.component.html',
  styleUrls: ['./search-flights.component.css']
})
export class SearchFormComponent {
  public flights;
  f: any[];
  tableResource: DataTableResource<Flight>;
  items: Flight[] = [];
  itemCount: number;
  weatherIcon;
  form = new FormGroup({
    sourceAirport: new FormControl('', Validators.required),
    destinationAirport: new FormControl('', Validators.required),
    destinationCity: new FormControl('', Validators.required),
    adults: new FormControl(),
    children: new FormControl(),
    departureDate: new FormControl('', Validators.required),
    arrivalDate: new FormControl()
  });
  constructor(private flightService: FlightService, private weatherSrvice: WeatherService) {}

  getFlights() {
    this.flightService.getFlights(this.form)
    .subscribe(response => {
      // this.setFlights(response);
      const mapped = Object.values(response.json()).map(value => ({
        type: value
      }));
      this.f = Object.values(mapped[0]);
      this.flights = Object.values(this.f[0])[1];
      this.initializeTable();
      this.initializeWeatherIcon();
    }, (error: Response) => {
      if (error.status === 400) {
        this.form.setErrors(error);
        console.log('Error Happen: \n' + error);
      } else {
        alert('An unexpected error occured, check logs for more details ');
        console.log('An unexpected error' + error);
      }
    });
  }

  private initializeTable() {
    this.tableResource = new DataTableResource(this.flights);
    this.tableResource.query({ offset: 0 }).then(items => (this.items = items));
    this.tableResource.count().then(count => (this.itemCount = count));
   }

   initializeWeatherIcon() {
    // this.weatherSrvice.getCurrentWeatherFor(this.form.get('destinationCity').value).subscribe(response => {
    //   this.weatherIcon = response;
    // });
    console.log(Math.floor(Math.random() * Math.floor(3)));
    this.weatherIcon = '../../assets/' + Math.floor(Math.random() * Math.floor(3)) + '.png';

   }

  reloadItems(params) {
    if (this.tableResource) {
      this.tableResource
        .query(params)
        .then(items => (this.items = items.slice()));
    }
  }

  get sourceAirport() {
    return this.form.get('sourceAirport');
  }

  get destinationAirport() {
    return this.form.get('destinationAirport');
  }

  get destinationCity() {
    return this.form.get('destinationCity');
  }

  get adults() {
    return this.form.get('adults');
  }

  get children() {
    return this.form.get('children');
  }

  get departureDate() {
    return this.form.get('departureDate');
  }

  get arrivalDate() {
    return this.form.get('arrivalDate');
  }
}
